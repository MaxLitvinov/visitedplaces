package com.teadust.visitedplaces;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends AppCompatActivity {

    private final int REFRESH_MIN_TIME = 15;
    private final int REFRESH_MIN_DISTANCE = 2;
    private final int ZOOM = 17;

    private int currentApi;

    private GoogleMap map;
    private LocationManager locationManager;
    private String providerName;
    private Location location;
    private LatLng coordinates;
    private Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        currentApi = Build.VERSION.SDK_INT;

        Criteria criteria = new Criteria();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        providerName = locationManager.getBestProvider(criteria, false);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showGpsAlertDialog();
        }

        // For support android 6.0 and higher
        if (currentApi >= 23) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
            }
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, REFRESH_MIN_TIME, REFRESH_MIN_DISTANCE, locationListener);

        location = locationManager.getLastKnownLocation(providerName);

        if (location != null) {
            coordinates = new LatLng(location.getLatitude(), location.getLongitude());
        } else {
            coordinates = new LatLng(0, 0);
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment)).getMap();
                marker = map.addMarker(new MarkerOptions().position(coordinates).title("You are here"));
                if (coordinates.latitude == 0 && coordinates.longitude == 0) {
                    marker.setTitle("Sharks everywhere!");
                }
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, ZOOM));
                map.animateCamera(CameraUpdateFactory.zoomTo(ZOOM), REFRESH_MIN_TIME, null);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (currentApi >= 23) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, REFRESH_MIN_TIME, REFRESH_MIN_DISTANCE, locationListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (currentApi >= 23) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        locationManager.removeUpdates(locationListener);
    }

    private android.location.LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
            marker.setPosition(currentLocation);
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, ZOOM));
            Log.d("coordinates: ", location.getLatitude() + ", " + location.getLongitude());
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private void showGpsAlertDialog() {
        AlertDialog.Builder gpsAlertDialog = new AlertDialog.Builder(this);

        gpsAlertDialog.setTitle("Neccessary to enable GPS! Enable?");

        gpsAlertDialog.setPositiveButton("Enable", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        gpsAlertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        gpsAlertDialog.show();
    }
}
